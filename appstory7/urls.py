from django.urls import path
from . import views
from story3.settings import DEBUG, STATIC_URL, STATIC_ROOT
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name = 'index'),

]