from django.shortcuts import render, redirect
from .forms import UserCreateForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from . import forms 
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django.urls import reverse_lazy
# Create your views here.

@login_required(login_url='/story9/login/')
def index(request):
    return render(request, 'index9.html')

class SignUp(CreateView):
    form_class = forms.UserCreateForm
    success_url = reverse_lazy('appstory9:login')
    template_name = 'signup.html'


