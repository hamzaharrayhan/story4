from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class TestSignIn(TestCase):
    # def test_event_url_is_exist(self):
    #     response = Client().get('/storysembilan/signin/')
    #     self.assertEqual(response.status_code, 200)

    # def test_event_func(self):
    #     found_func = resolve('/storysembilan/signin/')
    #     self.assertEqual(found_func.func, signin)

    # def test_event_using_template(self):
    #     template = Client().get('/storysembilan/signin/')
    #     self.assertTemplateUsed(template, 'signin.html')

    def test_signin_with_new_account(self):
        c = Client()

        user = User.objects.create(
            username='rayhan.akbar1', email='rayhan.akbar207@gmail.com')
        user.set_password('2.5Mequh.tQH$!8')
        user.save()

        logged_in = c.login(username='rayhan.akbar1',
                            password='2.5Mequh.tQH$!8')

        self.assertTrue(logged_in)
