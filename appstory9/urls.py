from django.urls import path
from . import views
from story3.settings import DEBUG, STATIC_URL, STATIC_ROOT
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

app_name = 'appstory9'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'),name='login'),
    path('logout/', auth_views.LogoutView.as_view(),name='logout'),
    path('signup/', views.SignUp.as_view(),name='signup'),
]