from django.urls import path
from . import views
from story3.settings import DEBUG, STATIC_URL, STATIC_ROOT
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name = 'index'),
    path('upload/', views.upload, name = 'upload-matkul'),
    path('update/<int:id_matkul>', views.update_matkul),
    path('delete/<int:id_matkul>', views.delete_matkul),
    path('detail/<int:id_matkul>', views.detail),
]