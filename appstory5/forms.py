from django import forms
from .models import Matkul

class ListMatkul(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'