# Generated by Django 3.1.1 on 2020-10-14 20:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstory5', '0002_auto_20201014_2041'),
    ]

    operations = [
        migrations.RenameField(
            model_name='matkul',
            old_name='name',
            new_name='nama',
        ),
    ]
