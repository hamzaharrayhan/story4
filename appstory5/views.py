from django.shortcuts import render
from django.shortcuts import render, redirect
from .models import Matkul
from .forms import ListMatkul
from django.http import HttpResponse
# Create your views here.

def index(request):
    dataMatkul = Matkul.objects.all()
    return render(request, 'Matkul/listMatkul.html', {'dataMatkul':dataMatkul})

def detail(request, id_matkul):
    id_matkul = int(id_matkul) 
    try:
        cek_matkul = Matkul.objects.get(id = id_matkul)
    except Matkul.DoesNotExist:
        return redirect('index')
    return render(request, 'Matkul/detailMatkul.html', {'cek_matkul': cek_matkul})

def upload(request):
    upload = ListMatkul()
    if request.method == 'POST':
        upload = ListMatkul(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('index')
        else:
            return HttpResponse("""Terjadi kesalahan pembuatan form, reload pada <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'Matkul/upload_form.html', {'upload_form':upload})

def update_matkul(request, id_matkul):
    id_matkul = int(id_matkul)
    try:
        cek_matkul = Matkul.objects.get(id = id_matkul)
    except Matkul.DoesNotExist:
        return redirect('index')
    upload = ListMatkul(request.POST or None, instance = cek_matkul)
    if upload.is_valid():
       upload.save()
       return redirect('index')
    return render(request, 'Matkul/upload_form.html', {'upload_form':upload})

def delete_matkul(request, id_matkul):
    id_matkul = int(id_matkul) 
    try:
        cek_matkul = Matkul.objects.get(id = id_matkul)
    except Matkul.DoesNotExist:
        return redirect('index')
    cek_matkul.delete()
    return redirect('index')

