from django.db import models

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField('Nama Matkul', max_length=264) 
    dosen = models.CharField('Nama Dosen', max_length=264)
    sks = models.IntegerField('Jumlah SKS')
    deskripsi = models.TextField('Deskripsi', default = 'Deskripsi mata kuliah')
    semester = models.CharField('Semester', max_length=264, default = 'Gasal 2020/2021') 
    ruang = models.CharField('Ruang Kelas', max_length=264)

    def __str__(self):
        return self.nama