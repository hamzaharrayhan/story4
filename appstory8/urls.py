from django.urls import path
from .views import *
from story3.settings import DEBUG, STATIC_URL, STATIC_ROOT
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name='index'),
    path('cari/', panggilAPI, name="panggil"),
]
