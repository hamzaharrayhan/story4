from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    response = {}
    return render(request,"index8.html",response)

def panggilAPI(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)

    data = json.loads(ret.content)
    return JsonResponse(data,safe=False)
