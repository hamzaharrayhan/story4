from django.urls import path
from . import views
from story3.settings import DEBUG, STATIC_URL, STATIC_ROOT
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name = 'index'),
    path('upload/', views.uploadKegiatan, name="uploadkegiatan"),
    path('uploadpeserta/', views.uploadPeserta,name="uploadpeserta"),
    path('delete/<int:id_kegiatan>/', views.hapusKegiatan),
]