from django.contrib import admin
from .models import Kegiatan, Peserta
from django.forms import ModelForm
# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Peserta)
