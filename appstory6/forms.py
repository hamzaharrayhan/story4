from django import forms
from .models import Kegiatan, Peserta
from django.forms import TextInput, Select, Textarea

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan',
        ]
        widgets = {
            'namaKegiatan': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama Kegiatan'}
            ),
        }
        

class FormPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
            'namaKegiatan',
        ]
        widgets = {
            'namaPeserta': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama Peserta',
                }
            ),
            'namaKegiatan': forms.Select(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama Kegiatan',}
            ),
        }
