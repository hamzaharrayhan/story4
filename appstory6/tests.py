# Create your tests here.

from django.test import TestCase, Client
from django.urls import resolve
from .views import index, uploadKegiatan, uploadPeserta, hapusKegiatan
from .models import Kegiatan, Peserta
from .apps import Appstory6Config

# Create your tests here.
class Test_App(TestCase):
    def test_app_name(self):
        self.assertEqual(Appstory6Config.name,'appstory6')

class TestKegiatan(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'index6.html')


class TestTambahKegiatan(TestCase):
    def test_add_event_url_is_exist(self):
        response = Client().post('/story6/upload/',data={'namaKegiatan': 'PPW'})
        self.assertEqual(response.status_code, 302)

    def test_add_event_index_func(self):
        found = resolve('/story6/upload/')
        self.assertEqual(found.func, uploadKegiatan)

    def test_add_event_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'index6.html')

    def test_event_model_create_new_object(self):
        kegiatan = Kegiatan(namaKegiatan="PPW")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/story6/upload/', data={'namaKegiatan': 'PPW'})
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(namaKegiatan="PPW")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_regist_POST(self):
        response = Client().post('/story6/uploadpeserta/',data={'namaPeserta': 'bangjago'})
        self.assertEqual(response.status_code, 200)

    def test_event_url_post_is_valid(self):
        response = Client().post(
            '/story6/uploadpeserta/', data={'namaPeserta': 'PPW'})
        self.assertEqual(response.status_code, 200)

class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = Kegiatan(namaKegiatan="Coding")
        acara.save()
        # nama = Peserta(namaPeserta="jace")
        # nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/story6/delete/1/')
        self.assertEqual(response.status_code, 302)

