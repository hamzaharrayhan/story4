from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import FormKegiatan, FormPeserta
from django.http import HttpResponse
# Create your views here.

def index(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    upload = FormKegiatan
    upload2 = FormPeserta
    return render(request, 'index6.html', {'kegiatans':kegiatan, 'pesertas':peserta, "uploadKegiatan":upload, "uploadPeserta":upload2})

def uploadKegiatan(request):
    if request.method == 'POST':
        upload = FormKegiatan(request.POST)
        if upload.is_valid():
            upload.save()
            return redirect('/story6')
       
        return HttpResponse("""Terjadi kesalahan pembuatan form, reload pada <a href = "{{ url : 'index'}}">reload</a>""")

def uploadPeserta(request):
    if request.method == 'POST':
        upload2 = FormPeserta(request.POST)
        if upload2.is_valid():
            # upload2(namaKegiatan = request.POST['NamaKegiatanForm'])
            upload2.save()
            return redirect('/story6')
        else:
            return HttpResponse("""Terjadi kesalahan pembuatan form, reload pada <a href = "{{ url : 'index'}}">reload</a>""")

def hapusKegiatan(request,id_kegiatan):
    id_kegiatan = int(id_kegiatan)
    try:
        getKegiatan = Kegiatan.objects.get(id = id_kegiatan)
    except Kegiatan.DoesNotExist:
        return redirect("/story6")
    
    getKegiatan.delete() 
    return redirect("/story6")
