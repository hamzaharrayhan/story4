from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    namaKegiatan = models.CharField('Nama Kegiatan ', max_length=264) 

    def __str__(self):
        return self.namaKegiatan
    
class Peserta(models.Model):
    namaPeserta = models.CharField('Nama Peserta ', max_length=264)
    namaKegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
        
    def __str__(self):
        return self.namaPeserta