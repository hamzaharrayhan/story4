from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('kosong/', views.kosong, name='kosong'),
    # dilanjutkan ...
]